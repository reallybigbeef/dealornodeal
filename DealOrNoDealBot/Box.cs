﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealOrNoDealBot
{
    public class Box
    {
        public double value;
        public BoxColor color;
        public int number;
        private bool _opened;
        public bool opened
        {
            get { return _opened; }
            set
            {
                _opened = value;
            }
        }
        public bool isPlayersBox;

        public Box(double cValue, int cNumber)
        {
            value = cValue;
            color = value > 100 ? BoxColor.Red : BoxColor.Blue;
            number = cNumber;
            opened = false;
            isPlayersBox = false;
        }
    }
}
