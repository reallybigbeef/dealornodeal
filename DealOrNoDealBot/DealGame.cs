﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net;
using Discord.WebSocket;

namespace DealOrNoDealBot
{
    public class DealGame
    {
        public List<double> values = new List<double> {
            0.01,
            0.10,
            0.50,
            1,
            5,
            10,
            50,
            100,
            250,
            500,
            750,
            1000,
            3000,
            5000,
            10000,
            15000,
            20000,
            35000,
            50000,
            75000,
            100000,
            250000
        };

        List<Box> boxes;

        public bool PlayerBoxSet;
        public int PlayersBoxNum;

        public bool PlayerHasDealt;
        public double dealAmount;
        public int boxesRemaining;

        public bool finalRound = false;

        public DealGame()
        {
            var rnd = new Random();
            var result = values.OrderBy(item => rnd.Next());
            values = result.ToList();

            boxes = new List<Box>();
            for (int i = 0; i < 22; i++)
            {
                boxes.Add(new Box(values[i], i));
            }
            PlayerBoxSet = false;
            PlayerHasDealt = false;
            boxesRemaining = 22;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <returns>
        /// first bool indicates whether there is a problem opening that box
        /// 2nd string is print message
        /// 3rd bool indicates if last box opened
        /// </returns>
        public Tuple<bool, string, bool> openBox(int number)
        {
            if (!boxes[number].opened)
            {
                if (!PlayerHasDealt)
                {
                    if (PlayerBoxSet)
                    {
                        if (number == PlayersBoxNum && !finalRound)
                        {
                            return new Tuple<bool, string, bool>(false, "You cant open your box yet!", false);
                        }
                        else if (finalRound && number == PlayersBoxNum)
                        {
                            boxes[PlayersBoxNum].opened = true;
                            return new Tuple<bool, string, bool>(false, $"Your box contains...\n\n\t\t\t{String.Format("{0:n}", boxes[PlayersBoxNum].value)}!!", true);
                        }
                        else
                        {
                            var box = boxes[number];
                            box.opened = true;
                            var openedValue = box.value;
                            boxesRemaining--;
                            if(boxesRemaining == 2)
                            {
                                finalRound = true;
                            }
                            return new Tuple<bool, string, bool>(true, $"You opened box {number}. It contains.....\n\t£{String.Format("{0:n}", openedValue)}", false);
                        }
                    }
                    else
                    {
                        return new Tuple<bool, string, bool>(false, "Set your box first!", false);
                    }
                }
                else if ((number == PlayersBoxNum))
                {
                    boxes[PlayersBoxNum].opened = true;
                    return new Tuple<bool, string, bool>(false, $"Your box contains...\n\n\t\t\t£{String.Format("{0:n}", boxes[PlayersBoxNum].value)}", true);
                }
                else
                {
                    return new Tuple<bool, string, bool>(false, $"You have already dealt!", false);
                }
            }
            else
            {
                return new Tuple<bool, string, bool>(false, "This box is open.", false);
            }
        }
        public string listValues()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("The boxes remaining are:\n");
            foreach (var item in boxes)
            {
                sb.Append($"Box {item.number}\t");
                var openstr = item.opened ? "Opened" : "Not Opened";
                sb.Append(openstr +"\t-");
                var content = item.opened ? '£'+item.value.ToString() : "?";
                sb.Append(content);
                sb.AppendLine();
            }

            var BoxesInOrder = boxes.OrderBy(x => x.value).ToList();
            sb.Append("\n--------------------------------\n");
            for (int i=0; i<11; i++)
            {
                var idx = i;
                for (int j = 0; j < 2; j++)
                {
                    if (j > 0) { idx = i + 11; }
                    sb.Append("|");
                    if (!BoxesInOrder[idx].opened)
                    {
                        sb.Append(PadCenter(BoxesInOrder[idx].value.ToString(), 15));
                    }
                    else
                    {
                        sb.Append(PadCenter("", 15));
                    }
                    sb.Append("\t|");
                    
                }
                sb.Append("\n--------------------------------\n");

            }

            return sb.ToString();
        }

        public string SetPlayerBox(int playerBoxNum)
        {
            if (!PlayerBoxSet)
            {
                PlayersBoxNum = playerBoxNum;
                boxes[playerBoxNum].isPlayersBox = true;
                PlayerBoxSet = true;
                return $"You chose box {playerBoxNum}";
            }
            else
            {
                return $"You've already set your box. It's box number {PlayersBoxNum}";
            }
        }

        public string Deal(double amount)
        {
            dealAmount = amount;
            PlayerHasDealt = true;
            return $"You have dealt! You walk away with £{String.Format("{0:n}", amount)}. But could you have got more?";
        }

        internal double CalculateOffer()
        {
            List<Box> nonOpenedBoxes = new List<Box>();
            foreach (var box in boxes)
            {
                if (!box.opened)
                {
                    nonOpenedBoxes.Add(box);
                }
            }
            
            var nonOpenedBoxesInOrder = nonOpenedBoxes.OrderBy(x => x.value ).ToList();

            float median = nonOpenedBoxesInOrder.Count / 2;
            //Median
            var middleValue = nonOpenedBoxesInOrder[(int)Math.Floor(median)].value;

            double total = 0;
            foreach (var box in nonOpenedBoxes)
            {
                total = total + box.value;
            }
            //Mean
            var mean = total / nonOpenedBoxes.Count();

            return mean;
        }


        private string PadCenter(string text, int newWidth)
        {
            const char filler = ' ';
            int length = text.Length;
            int charactersToPad = newWidth - length;
            if (charactersToPad < 0) throw new ArgumentException("New width must be greater than string length.", "newWidth");
            int padLeft = charactersToPad / 2 + charactersToPad % 2;
            //add a space to the left if the string is an odd number
            int padRight = charactersToPad / 2;

            StringBuilder resultBuilder = new StringBuilder(newWidth);
            for (int i = 0; i < padLeft; i++) resultBuilder.Insert(i, filler);
            for (int i = 0; i < length; i++) resultBuilder.Insert(i + padLeft, text[i]);
            for (int i = newWidth - padRight; i < newWidth; i++) resultBuilder.Insert(i, filler);
            return resultBuilder.ToString();
        }


    }
}