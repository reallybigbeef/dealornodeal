﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net;
using Discord.WebSocket;
using System.Diagnostics;
using Discord.Audio;
using System.IO;
using System.Threading;

namespace DealOrNoDealBot
{
    class DealOrNoDealBot
    {
        public event EventHandler MsgFromConsole;
        bool firstRound = false;
        const string token = "NDczMTcyMzA3NDYxMDEzNTE3.Dj-EIg.-qJk3JMySk08e6bQR33o8gvaXpc";
        public SocketMessage message;
        public DealGame _game;
        IAudioClient audio;
        SocketChannel voiceChannel;
        public int boxesOpenedThisRound;

        public DealOrNoDealBot()
        {
            _client = new DiscordSocketClient();
            boxesOpenedThisRound = 0;
        }
        private DiscordSocketClient _client;
        private bool BankerTime;
        private double offer;

        public async Task MainAsync()
        {
            _client.Log += Log;
            _client.MessageReceived += MessageReceived;
            _client.UserLeft += UserLeft;
            MsgFromConsole += writeConsoleText;
            _client.Ready += _client_Ready;
            _game = new DealGame();

            // Remember to keep this private!
            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();


            // Block this task until the program is closed.
            await Task.Delay(-1);

        }

        private async Task _client_Ready()
        {


            //var channel = _client.GetChannel(389574782024417291);
            //var q = channel as SocketTextChannel;
            //q.SendMessageAsync("yo fuck boi's I'm watching chat so if one of you heckin frigger's swear, there will be frigging load of heck to pay");
            
            //var channel = _client.GetChannel(471402580497530890);
            //voiceChannel = _client.GetChannel(466743179752898560);

            //var p = voiceChannel as SocketVoiceChannel;
            //audio = await p.ConnectAsync();

            //using (var ffmpeg = CreateProcess(@"C:\Users\clark\Downloads\DealOrNoDealThemeTune.mp3"))
            //using (var stream = audio.CreatePCMStream(AudioApplication.Music))
            //{
            //    try { await ffmpeg.StandardOutput.BaseStream.CopyToAsync(stream); }

            //    finally { await stream.FlushAsync(); }
            //}

            //var q = channel as SocketTextChannel;
            //for (int i = 0; i < 10; i++)
            //{
            //    q.SendMessageAsync("yo fuck boi's I'm watching chat so if one of you heckin frigger's swear, there will be frigging load of heck to pay");
            //}


        }

        private void writeConsoleText(object sender, EventArgs e)
        {

        
        }

        private async Task UserLeft(SocketGuildUser arg)
        {
            await arg.SendMessageAsync("Good riddence ye fuck" + arg.Nickname.ToString());
        }

        private async Task MessageReceived(SocketMessage message)
        {
            //Kick User
            //var y = message.Author as SocketGuildUser;
            //var x = (_client.GetUser("Dyno", "3861") as SocketGuildUser);
            ////await x.KickAsync();
            //await y.KickAsync();


            this.message = message;
            if (message.Content == "!ping")
            {
                await message.Channel.SendMessageAsync("Pong!");
            }
            if (message.Content == "!rules")
            {
                Console.WriteLine(message.Channel.GetType().ToString());
                await message.Channel.SendMessageAsync("23 Boxes. 1 Question. Deal, or No Deal?");
            }
            if (message.Content.ToLower().Contains("!openbox"))
            {
                try
                {
                    if (!BankerTime)
                    {
                        var msg = message.Content.Substring(9, message.Content.Length - 9);
                        var boxNum = int.Parse(msg);
                        var output = _game.openBox(boxNum);

                        await message.Channel.SendMessageAsync(output.Item2);
                        if (output.Item1)
                        {
                            boxesOpenedThisRound++;
                            
                            if ((!firstRound && boxesOpenedThisRound == 3) || (firstRound && boxesOpenedThisRound == 5))
                            {
                                firstRound = false;
                                boxesOpenedThisRound = 0;
                                BankerTime = true;
                                CallTheBanker(message);
                            }
                        }
                        CheckForValueSounds(output.Item2);
                    }
                    else
                    {
                        await message.Channel.SendMessageAsync("Answer the banker first! Deal? or No Deal?");
                    }

                }
                catch (Exception ex)
                {
                    await message.Channel.SendMessageAsync(ex.Message);
                }
            }
            if (message.Content.ToLower() == "!status")
            {
                await message.Channel.SendMessageAsync(_game.listValues());
            }
            if (message.Content.ToLower() == "!start")
            {
                _game = new DealGame();
                firstRound = true;
                await message.Channel.SendMessageAsync("Welcome to Deal or No Deal!");
                StartMusic(@"C:\Users\clark\Downloads\DealOrNoDealThemeTune.mp3");
            }
            if (message.Content == "!song")
            {
                await audio.StopAsync();
            }
            if(message.Content == "!intro")
            {
                StartMusic(@"C:\Users\clark\Downloads\DealOrNoDealIntro.mp3");
            }
            if (message.Content.ToUpper().Contains("!DEAL") && BankerTime)
            {
                await message.Channel.SendMessageAsync(_game.Deal(offer));
                
                StartMusic(@"C:\Users\clark\Downloads\Deal.mp3");
                BankerTime = false;
            }
            if ((message.Content.ToUpper().Contains("!NO DEAL") || message.Content.ToUpper().Contains("!NODEAL"))&& BankerTime)
            {
                await message.Channel.SendMessageAsync("NO DEAL!");
                StartMusic(@"C:\Users\clark\Downloads\No_DEAL.mp3");
                BankerTime = false;
            }
            if (message.Content.ToUpper().Contains("!SETBOX"))
            {
                try
                {
                    var msg = message.Content.Substring(8, message.Content.Length - 8);
                    var boxNum = int.Parse(msg);
                    var output = _game.SetPlayerBox(boxNum);
                    await message.Channel.SendMessageAsync(output);
                }
                catch (Exception ex)
                {
                    await message.Channel.SendMessageAsync(ex.Message);
                }
            }
        }

        private void CheckForValueSounds(string item2)
        {
            if (item2.Contains("250,000.00") && item2.Contains("You opened box"))
            {
                StartMusic(@"C:\Users\clark\Downloads\What_are_you_doing.mp3");
            }
            else if (item2.Contains("15,000.00") && item2.Contains("You opened box"))
            {
                StartMusic(@"C:\Users\clark\Downloads\15000.mp3");
            }
        }

        private async void CallTheBanker(SocketMessage message)
        {
            await message.Channel.SendMessageAsync("The bankers calling....");
            StartMusic(@"C:\Users\clark\Downloads\Ring.mp3");
            Thread.Sleep(2000);
            offer = CalculateOffer();
            await message.Channel.SendMessageAsync($"The banker offers: \n\t\t£{String.Format("{0:n}", offer)}\nDeal? Or No Deal?");
            
        }

        private double CalculateOffer()
        {
            double amount = _game.CalculateOffer();
            return amount;
        }

        private async void StartMusic(string path)
        {
            try
            {
                var channel = _client.GetChannel(389574782024417291);
                voiceChannel = _client.GetChannel(531505474642771986);
                if (audio != null)
                {
                    await audio.StopAsync();
                }
                var p = voiceChannel as SocketVoiceChannel;
                audio = await p.ConnectAsync();

                using (var ffmpeg = CreateProcess(path))
                using (var stream = audio.CreatePCMStream(AudioApplication.Music))
                {
                    try { await ffmpeg.StandardOutput.BaseStream.CopyToAsync(stream); }

                    finally { await stream.FlushAsync(); }
                }
            }
            catch (Exception)
            {

            }

        }

        private Process CreateProcess(string path)
        {
            return Process.Start(new ProcessStartInfo
            {
                FileName = @"C:\ffmpeg\bin\ffmpeg.exe",
                Arguments = $"-hide_banner -loglevel panic -i \"{path}\" -ac 2 -f s16le -ar 48000 pipe:1",
                UseShellExecute = false,
                RedirectStandardOutput = true
            });
        }


        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}
