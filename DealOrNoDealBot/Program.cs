﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.Net;

namespace DealOrNoDealBot
{
    class Program
    {
        
        static void Main(string[] args)
             => new DealOrNoDealBot().MainAsync().GetAwaiter().GetResult();
        
    }
}
